package by.shpakovsky.hotelsservice.dao;

import java.util.List;

import by.shpakovsky.hotelsservice.data.Room;

public interface RoomDAO {
	
	List<Room> readhotelRooms(int hotelId);
	Room readRoom(int id);
}
