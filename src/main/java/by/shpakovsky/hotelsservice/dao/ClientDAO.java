package by.shpakovsky.hotelsservice.dao;

import by.shpakovsky.hotelsservice.data.Client;

public interface ClientDAO {
	
	int createClient(Client client);
	Client readClient(int clientId);
	void updateClient(Client client);
	
}
