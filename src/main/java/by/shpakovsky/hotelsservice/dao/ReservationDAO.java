package by.shpakovsky.hotelsservice.dao;

import java.util.List;

import by.shpakovsky.hotelsservice.data.Reservation;

public interface ReservationDAO {
	
	int createReservation(Reservation reservation);
	Reservation readReservation(int id);
	List<Reservation> readRoomReservations(int roomId);
	List<Reservation> readClientReservations(int clientId);
	void deleteReservation(int id);
	
}
