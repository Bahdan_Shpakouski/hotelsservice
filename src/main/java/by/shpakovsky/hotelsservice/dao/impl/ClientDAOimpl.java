package by.shpakovsky.hotelsservice.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import by.shpakovsky.hotelsservice.dao.ClientDAO;
import by.shpakovsky.hotelsservice.data.Client;

public class ClientDAOimpl implements ClientDAO {

	private static final String SELECT = "select * from client where clientid=?";
	private static final String UPDATE = "update client set name=?, "
			+ "surname=?, age=?, mail=?, gender=? where clientid=?";
	private static final String CREATE = "insert into client (clientid, name, surname,"
			+ "age, mail, gender) values (?, ?, ?, ?, ?, ?)";
	
	@Autowired
	private Session session;
	
	public ClientDAOimpl() {
	}

	@Override
	public int createClient(Client client) {
		PreparedStatement prepared = session.prepare(CREATE);
		BoundStatement bound = prepared.bind(client.getClientId(), client.getName(),
				client.getSurname(), client.getAge(), client.getMail(), client.getGender());
		session.execute(bound);
		return client.getClientId();
	}

	@Override
	public Client readClient(int clientId) {
		PreparedStatement prepared = session.prepare(SELECT);
		BoundStatement bound = prepared.bind(clientId);
		ResultSet result = session.execute(bound);
		Client client = new Client();
		while(!result.isExhausted()){
			Row row = result.one();
			client.setClientId(row.getInt("clientid"));
			client.setName(row.getString("name"));
			client.setSurname(row.getString("surname"));
			client.setAge(row.getInt("age"));
			client.setMail(row.getString("mail"));
			client.setGender(row.getString("gender"));
		}
		return client;
	}

	@Override
	public void updateClient(Client client) {
		PreparedStatement prepared = session.prepare(UPDATE);
		BoundStatement bound = prepared.bind(client.getName(), client.getSurname(), 
				client.getAge(), client.getMail(), client.getGender(), client.getClientId());
		session.execute(bound);
	}

}
