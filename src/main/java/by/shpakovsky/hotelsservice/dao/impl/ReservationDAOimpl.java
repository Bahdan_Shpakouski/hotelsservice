package by.shpakovsky.hotelsservice.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import by.shpakovsky.hotelsservice.dao.ReservationDAO;
import by.shpakovsky.hotelsservice.data.Reservation;

public class ReservationDAOimpl implements ReservationDAO {

	private static final String SELECT = "select * from reservation where reservationid=?";
	private static final String SELECT_ROOM_RESERVATIONS = "select * from reservation where roomid=?";
	private static final String SELECT_CLIENT_RESERVATIONS = "select * from reservation where clientid=?";
	private static final String CREATE = "insert into reservation (reservationid, clientid,"
			+ " roomid, numberofdaysstay, arrivaldate) values (?, ?, ?, ?, ?)";
	private static final String DELETE = "delete from reservation where reservationid=?";
	
	@Autowired
	private Session session;
	
	public ReservationDAOimpl() {
		
	}

	@Override
	public int createReservation(Reservation reservation) {
		PreparedStatement prepared = session.prepare(CREATE);
		BoundStatement bound = prepared.bind(reservation.getReservationId(),
				reservation.getClientId(), reservation.getRoomId(), 
				reservation.getNumberOfDaysStay(), reservation.getArrivalDate());
		session.execute(bound);
		return reservation.getReservationId();
	}

	@Override
	public Reservation readReservation(int id) {
		ResultSet result = session.execute(SELECT);
		Reservation reservation = new Reservation();
		while(!result.isExhausted()){
			Row row = result.one();
			reservation.setReservationId(row.getInt("reservationid"));
			reservation.setClientId(row.getInt("clientid"));
			reservation.setRoomId(row.getInt("roomid"));
			reservation.setNumberOfDaysStay(row.getInt("numberofdaysstay"));
			reservation.setArrivalDate(row.getTimestamp("arrivaldate"));
		}
		return reservation;
	}
	
	@Override
	public List<Reservation> readRoomReservations(int roomId) {
		List<Reservation> reservations = new ArrayList<Reservation>();
		PreparedStatement prepared = session.prepare(SELECT_ROOM_RESERVATIONS);
		BoundStatement bound = prepared.bind(roomId);
		ResultSet reservationResult = session.execute(bound);
		List<Row> rows = reservationResult.all();
		for(Row reservationRow : rows){
			Reservation reservation = new Reservation();
			reservation.setReservationId(reservationRow.getInt("reservationid"));
			reservation.setClientId(reservationRow.getInt("clientid"));
			reservation.setRoomId(reservationRow.getInt("roomid"));
			reservation.setNumberOfDaysStay(reservationRow.getInt("numberofdaysstay"));
			reservation.setArrivalDate(reservationRow.getTimestamp("arrivaldate"));
			reservations.add(reservation);
		}
		return reservations;
	}

	@Override
	public List<Reservation> readClientReservations(int clientId) {
		List<Reservation> reservations = new ArrayList<Reservation>();
		PreparedStatement prepared = session.prepare(SELECT_CLIENT_RESERVATIONS);
		BoundStatement bound = prepared.bind(clientId);
		ResultSet reservationResult = session.execute(bound);
		List<Row> resRows = reservationResult.all();
		for(Row reservationRow : resRows){
			Reservation reservation = new Reservation();
			reservation.setReservationId(reservationRow.getInt("reservationid"));
			reservation.setClientId(reservationRow.getInt("clientid"));
			reservation.setRoomId(reservationRow.getInt("roomid"));
			reservation.setNumberOfDaysStay(reservationRow.getInt("numberofdaysstay"));
			reservation.setArrivalDate(reservationRow.getTimestamp("arrivaldate"));
			reservations.add(reservation);
		}
		return reservations;
	}

	@Override
	public void deleteReservation(int id) {
		PreparedStatement prepared = session.prepare(DELETE);
		BoundStatement bound = prepared.bind(id);
		session.execute(bound);
	}

}
