package by.shpakovsky.hotelsservice.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import by.shpakovsky.hotelsservice.dao.HotelDAO;
import by.shpakovsky.hotelsservice.dao.RoomDAO;
import by.shpakovsky.hotelsservice.data.Hotel;
import by.shpakovsky.hotelsservice.data.Room;

public class HotelDAOimpl implements HotelDAO {

	private static final String SELECT_ALL = "select * from hotel";
	private static final String SELECT = "select * from hotel where hotelid=?";
	
	@Autowired
	private Session session;
	@Autowired
	private RoomDAO roomDAO;
	
	public HotelDAOimpl() {
		
	}

	@Override
	public List<Hotel> readHotels() {
		ResultSet result = session.execute(SELECT_ALL);
		List<Row> rows = result.all();
		List<Hotel> hotels = new ArrayList<Hotel>();
		for(Row row : rows){
			Hotel hotel = new Hotel();
			hotel.setHotelId(row.getInt("hotelid"));
			hotel.setHotelName(row.getString("hotelname"));
			hotel.setCity(row.getString("city"));
			hotel.setAddress(row.getString("address"));
			hotel.setStars(row.getInt("stars"));
			List<Room> rooms = roomDAO.readhotelRooms(hotel.getHotelId());
			hotel.setRooms(rooms);
			hotels.add(hotel);
		}
		return hotels;
	}

	@Override
	public Hotel readHotel(int id) {
		PreparedStatement prepared = session.prepare(SELECT);
		BoundStatement bound = prepared.bind(id);
		ResultSet result = session.execute(bound);
		Hotel hotel = new Hotel();
		while(!result.isExhausted()){
			Row row = result.one();
			hotel.setHotelId(row.getInt("hotelid"));
			hotel.setHotelName(row.getString("hotelname"));
			hotel.setCity(row.getString("city"));
			hotel.setAddress(row.getString("address"));
			hotel.setStars(row.getInt("stars"));
			List<Room> rooms = roomDAO.readhotelRooms(hotel.getHotelId());
			hotel.setRooms(rooms);
		}
		return hotel;
	}

}
