package by.shpakovsky.hotelsservice.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import by.shpakovsky.hotelsservice.dao.ReservationDAO;
import by.shpakovsky.hotelsservice.dao.RoomDAO;
import by.shpakovsky.hotelsservice.data.Reservation;
import by.shpakovsky.hotelsservice.data.Room;

public class RoomDAOimpl implements RoomDAO {

	private static final String SELECT_HOTEL_ROOMS = "select * from room where hotelid=?";
	private static final String SELECT = "select * from room where roomid=?";
	
	@Autowired
	private Session session;
	@Autowired
	private ReservationDAO reservationDAO;
	
	public RoomDAOimpl() {
		
	}

	@Override
	public List<Room> readhotelRooms(int hotelId) {
		PreparedStatement prepared = session.prepare(SELECT_HOTEL_ROOMS);
		BoundStatement bound = prepared.bind(hotelId);
		ResultSet result = session.execute(bound);
		List<Row> rows = result.all();
		List<Room> rooms = new ArrayList<Room>();
		for(Row row : rows){
			Room room = new Room();
			room.setRoomId(row.getInt("roomid"));
			room.setHotelId(row.getInt("hotelid"));
			room.setRoomNumber(row.getInt("roomnumber"));
			room.setRoomType(row.getString("roomtype"));
			room.setCost(row.getDouble("cost"));
			List<Reservation> reservations = reservationDAO.readRoomReservations(room.getRoomId());
			room.setReservations(reservations);
			rooms.add(room);
		}
		return rooms;
	}

	@Override
	public Room readRoom(int id) {
		PreparedStatement prepared = session.prepare(SELECT);
		BoundStatement bound = prepared.bind(id);
		ResultSet result = session.execute(bound);
		Room room = new Room();
		while(!result.isExhausted()){
			Row row = result.one();
			room.setRoomId(row.getInt("roomid"));
			room.setHotelId(row.getInt("hotelid"));
			room.setRoomNumber(row.getInt("roomnumber"));
			room.setRoomType(row.getString("roomtype"));
			room.setCost(row.getDouble("cost"));
			List<Reservation> reservations = reservationDAO.readRoomReservations(room.getRoomId());
			room.setReservations(reservations);
		}
		return room;
	}

}
