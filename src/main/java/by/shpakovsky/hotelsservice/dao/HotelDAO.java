package by.shpakovsky.hotelsservice.dao;

import java.util.List;

import by.shpakovsky.hotelsservice.data.Hotel;

public interface HotelDAO {

	List<Hotel> readHotels();
	Hotel readHotel(int id);
	
}
