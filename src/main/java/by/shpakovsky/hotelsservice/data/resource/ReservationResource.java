package by.shpakovsky.hotelsservice.data.resource;

import java.util.Date;

import org.springframework.hateoas.ResourceSupport;

public class ReservationResource extends ResourceSupport{

	public ReservationResource() {}
	
	private int reservationId;
	private int roomId;
	private int clientId;
	private Date arrivalDate;
	private int numberOfDaysStay;
	
	public int getReservationId() {
		return reservationId;
	}
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public int getNumberOfDaysStay() {
		return numberOfDaysStay;
	}
	public void setNumberOfDaysStay(int numberOfDaysStay) {
		this.numberOfDaysStay = numberOfDaysStay;
	}

}
