package by.shpakovsky.hotelsservice.data.resource;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;

public class RoomResource extends ResourceSupport{

	public RoomResource() {}
	
	private int roomId;
	private int hotelId;
	private int roomNumber;
	private String roomType;
	private double cost;
	
	private Resources<ReservationResource> reservations;

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public int getHotelId() {
		return hotelId;
	}

	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public Resources<ReservationResource> getReservations() {
		return reservations;
	}

	public void setReservations(Resources<ReservationResource> reservations) {
		this.reservations = reservations;
	}

	
}
