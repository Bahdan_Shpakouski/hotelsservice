package by.shpakovsky.hotelsservice.data.resource;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;


public class HotelResource extends ResourceSupport{
	
	public HotelResource() {}
	
	private int hotelId;
	private String hotelName;
	private String city;
	private String address;
	private int stars;
	private Resources<RoomResource> rooms;
	
	public int getHotelId() {
		return hotelId;
	}
	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}
	public String getHotelName() {
		return hotelName;
	}
	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
	public Resources<RoomResource> getRooms() {
		return rooms;
	}
	public void setRooms(Resources<RoomResource> rooms) {
		this.rooms = rooms;
	}

}
