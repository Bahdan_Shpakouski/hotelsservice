package by.shpakovsky.hotelsservice.data.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Link;

import by.shpakovsky.hotelsservice.controller.HotelController;
import by.shpakovsky.hotelsservice.data.Client;
import by.shpakovsky.hotelsservice.data.resource.ClientResource;

public class ClientAssembler extends ResourceAssemblerSupport<Client, ClientResource>{

	public ClientAssembler(Class<HotelController> controllerClass, Class<ClientResource> resourceType) {
		super(controllerClass, resourceType);
	}

	@Override
	public ClientResource toResource(Client client) {
		ClientResource clientResource = instantiateResource(client);
		clientResource.setClientId(client.getClientId());
		clientResource.setName(client.getName());
		clientResource.setSurname(client.getSurname());
		clientResource.setAge(client.getAge());
		clientResource.setGender(client.getGender());
		clientResource.setMail(client.getMail());
		clientResource.add(linkTo(methodOn(HotelController.class)
				.showClient(clientResource.getClientId())).withSelfRel());
		clientResource.add(linkTo(methodOn(HotelController.class).showHotels()).withRel("root"));
		//reservations
		//update
		return clientResource;
	}

	public Link getSelfLink(int clientId){
		Link link = linkTo(methodOn(HotelController.class)
				.showClient(clientId)).withSelfRel();
		return link;
	}
}
