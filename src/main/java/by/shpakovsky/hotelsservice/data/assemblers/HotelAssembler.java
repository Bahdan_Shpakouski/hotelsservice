package by.shpakovsky.hotelsservice.data.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import by.shpakovsky.hotelsservice.controller.HotelController;
import by.shpakovsky.hotelsservice.data.Hotel;
import by.shpakovsky.hotelsservice.data.resource.HotelResource;
import by.shpakovsky.hotelsservice.data.resource.RoomResource;

public class HotelAssembler extends ResourceAssemblerSupport<Hotel, HotelResource>{

	public HotelAssembler(Class<HotelController> controllerClass, Class<HotelResource> resourceType) {
		super(controllerClass, resourceType);
	}

	@Override
	public HotelResource toResource(Hotel hotel) {
		HotelResource hotelResource = instantiateResource(hotel);
		hotelResource.setHotelId(hotel.getHotelId());
		hotelResource.setHotelName(hotel.getHotelName());
		hotelResource.setAddress(hotel.getAddress());
		hotelResource.setCity(hotel.getCity());
		hotelResource.setStars(hotel.getStars());
		RoomAssembler assembler = new RoomAssembler(HotelController.class, RoomResource.class);
		List<RoomResource> resources = assembler.toResources(hotel.getRooms());
		Resources<RoomResource> wrapped = new Resources<RoomResource>(resources);
		hotelResource.setRooms(wrapped);
		hotelResource.add(linkTo(methodOn(HotelController.class)
				.showHotel(hotelResource.getHotelId())).withSelfRel());
		hotelResource.add(linkTo(methodOn(HotelController.class)
				.showHotelRooms(hotelResource.getHotelId())).withRel("rooms"));
		hotelResource.add(linkTo(methodOn(HotelController.class).showHotels()).withRel("root"));
		//registrate
		return hotelResource;
	}


}
