package by.shpakovsky.hotelsservice.data.assemblers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import by.shpakovsky.hotelsservice.controller.HotelController;
import by.shpakovsky.hotelsservice.data.Room;
import by.shpakovsky.hotelsservice.data.resource.ReservationResource;
import by.shpakovsky.hotelsservice.data.resource.RoomResource;

public class RoomAssembler extends ResourceAssemblerSupport<Room, RoomResource>{

	public RoomAssembler(Class<HotelController> controllerClass, Class<RoomResource> resourceType) {
		super(controllerClass, resourceType);
	}

	@Override
	public RoomResource toResource(Room room) {
		RoomResource roomResource = instantiateResource(room);
		roomResource.setRoomId(room.getRoomId());
		roomResource.setHotelId(room.getHotelId());
		roomResource.setRoomNumber(room.getRoomNumber());
		roomResource.setCost(room.getCost());
		roomResource.setRoomType(room.getRoomType());
		ReservationAssembler assembler = new ReservationAssembler(HotelController.class,
				ReservationResource.class);
		List<ReservationResource> resources = assembler.toResources(room.getReservations());
		Resources<ReservationResource> wrapped = new Resources<ReservationResource>(resources);
		roomResource.setReservations(wrapped);
		roomResource.add(linkTo(methodOn(HotelController.class)
				.showRoom(room.getHotelId(), room.getRoomId())).withSelfRel());
		roomResource.add(linkTo(methodOn(HotelController.class).showHotels()).withRel("root"));
		roomResource.add(linkTo(methodOn(HotelController.class)
				.showHotelRooms(roomResource.getHotelId())).withRel("rooms"));
		roomResource.add(linkTo(methodOn(HotelController.class).showHotel(room.getHotelId()))
				.withRel("hotel"));
		roomResource.add(linkTo(methodOn(HotelController.class)
				.showRoomReservations(room.getHotelId(), 
						room.getRoomId())).withRel("reservations"));
		//book room
		return roomResource;
	}

	

}
