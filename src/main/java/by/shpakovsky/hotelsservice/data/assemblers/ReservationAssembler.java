package by.shpakovsky.hotelsservice.data.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import by.shpakovsky.hotelsservice.controller.HotelController;
import by.shpakovsky.hotelsservice.data.Reservation;
import by.shpakovsky.hotelsservice.data.resource.ReservationResource;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.Link;

public class ReservationAssembler extends ResourceAssemblerSupport<Reservation, ReservationResource>{

	public ReservationAssembler(Class<HotelController> controllerClass,
			Class<ReservationResource> resourceType) {
		super(controllerClass, resourceType);
	}

	@Override
	public ReservationResource toResource(Reservation reservation) {
		ReservationResource reservationResource = instantiateResource(reservation);
		reservationResource.setReservationId(reservation.getReservationId());
		reservationResource.setClientId(reservation.getClientId());
		reservationResource.setArrivalDate(reservation.getArrivalDate());
		reservationResource.setNumberOfDaysStay(reservation.getNumberOfDaysStay());
		reservationResource.setRoomId(reservation.getRoomId());
		reservationResource.add(linkTo(methodOn(HotelController.class).showHotels()).withRel("root"));
		reservationResource.add(linkTo(methodOn(HotelController.class)
				.showClient(reservationResource.getClientId())).withRel("client"));
		//delete
		return reservationResource;
	}

	public Link getSelfLink(int hotelId, int roomId, int reservationId){
		Link link = linkTo(methodOn(HotelController.class)
				.showReservation(hotelId, roomId, reservationId)).withSelfRel();
		return link;
	}
	
	public Link getRoomLink(int hotelId, int roomId){
		Link link = linkTo(methodOn(HotelController.class)
				.showRoom(hotelId, roomId)).withRel("room");
		return link;
	}
	
	public Link getReservationsLink(int hotelId, int roomId){
		Link link = linkTo(methodOn(HotelController.class)
				.showRoomReservations(hotelId, roomId)).withRel("reservations");
		return link;
	}

}
