package by.shpakovsky.hotelsservice.data;

import java.util.Date;

public class Reservation {

	public Reservation() {
		
	}

	private int reservationId;
	private int roomId;
	private int clientId;
	private Date arrivalDate;
	private int numberOfDaysStay;
	
	public int getReservationId() {
		return reservationId;
	}
	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public Date getArrivalDate() {
		return arrivalDate;
	}
	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	public int getNumberOfDaysStay() {
		return numberOfDaysStay;
	}
	public void setNumberOfDaysStay(int numberOfDaysStay) {
		this.numberOfDaysStay = numberOfDaysStay;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((arrivalDate == null) ? 0 : arrivalDate.hashCode());
		result = prime * result + clientId;
		result = prime * result + numberOfDaysStay;
		result = prime * result + reservationId;
		result = prime * result + roomId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (arrivalDate == null) {
			if (other.arrivalDate != null)
				return false;
		} else if (!arrivalDate.equals(other.arrivalDate))
			return false;
		if (clientId != other.clientId)
			return false;
		if (numberOfDaysStay != other.numberOfDaysStay)
			return false;
		if (reservationId != other.reservationId)
			return false;
		if (roomId != other.roomId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Reservation [reservationId=" + reservationId + ", roomId=" + roomId + ", clientId=" + clientId
				+ ", arrivalDate=" + arrivalDate + ", numberOfDaysStay=" + numberOfDaysStay + "]";
	}
	
}
