package by.shpakovsky.hotelsservice.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@RestController
public class HelloController {

	private static final String HELLO = "Hello from %s!";
	private static final String MESSAGE = "This is demo program.";
	
	public HelloController() {	}
	
	@RequestMapping("/greeting")
    public HttpEntity<Greeting> greeting(@RequestParam(value = "name",
    required = false, defaultValue = "HotelsService") String name, @RequestParam(value = "msg",
    required = false, defaultValue = MESSAGE) String msg) {

        Greeting greeting = new Greeting(String.format(HELLO, name), msg);
        greeting.add(linkTo(methodOn(HelloController.class).greeting(name, msg)).withSelfRel());

        return new ResponseEntity<Greeting>(greeting, HttpStatus.OK);
    }
	
	public class Greeting extends ResourceSupport {

	    private final String content;
	    private final String message;

	    @JsonCreator
	    public Greeting(@JsonProperty("content") String content,
	    		@JsonProperty("message") String message) {
	        this.content = content;
	        this.message = message;
	    }

	    public String getContent() {
	        return content;
	    }
	    
	    public String getMessage() {
	        return message;
	    }
	}
}
