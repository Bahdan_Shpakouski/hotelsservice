package by.shpakovsky.hotelsservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import by.shpakovsky.hotelsservice.dao.*;
import by.shpakovsky.hotelsservice.data.*;
import by.shpakovsky.hotelsservice.data.assemblers.*;
import by.shpakovsky.hotelsservice.data.resource.*;

@RestController
public class HotelController {

	@Autowired
	private HotelDAO hotelDAO;
	@Autowired
	private RoomDAO roomDAO;
	@Autowired
	private ReservationDAO reservationDAO;
	@Autowired
	private ClientDAO clientDAO;
	
	public HotelController() {	}
	
	@RequestMapping("/hotels")
    public HttpEntity<List<HotelResource>> showHotels() {
		
		List<Hotel> hotels = hotelDAO.readHotels();
		HotelAssembler assembler = new HotelAssembler(HotelController.class,
				HotelResource.class);
		List<HotelResource> resources = assembler.toResources(hotels);
        return new ResponseEntity<List<HotelResource>>(resources, HttpStatus.OK);
    }
	
	@RequestMapping("/hotels/{hotel_id}")
	public HttpEntity<HotelResource> showHotel(@PathVariable int id){
		Hotel hotel = hotelDAO.readHotel(id);
		HotelAssembler assembler = new HotelAssembler(HotelController.class,
				HotelResource.class);
		HotelResource resource = assembler.toResource(hotel);
		return new ResponseEntity<HotelResource>(resource, HttpStatus.OK);
	}
	
	@RequestMapping("/hotels/{hotel_id}/rooms")
	public HttpEntity<List<RoomResource>> showHotelRooms(@PathVariable int id){
		List<Room> rooms = roomDAO.readhotelRooms(id);
		RoomAssembler assembler = new RoomAssembler(HotelController.class, RoomResource.class);
		List<RoomResource> resources = assembler.toResources(rooms);
		return new ResponseEntity<List<RoomResource>>(resources, HttpStatus.OK);
	}
	
	@RequestMapping("/hotels/{hotel_id}/rooms/{room_id}")
	public HttpEntity<RoomResource> showRoom(@PathVariable int hotelId, @PathVariable int roomId){
		Room room = roomDAO.readRoom(roomId);
		RoomAssembler assembler = new RoomAssembler(HotelController.class, RoomResource.class);
		RoomResource roomResource = assembler.toResource(room);
		return new ResponseEntity<RoomResource>(roomResource, HttpStatus.OK);
	}
	
	@RequestMapping("/hotels/{hotel_id}/rooms/{room_id}/reservations")
	public HttpEntity<List<ReservationResource>> showRoomReservations(@PathVariable int hotelId,
			@PathVariable int roomId){
		List<Reservation> reservations = reservationDAO.readRoomReservations(roomId);
		ReservationAssembler assembler = new ReservationAssembler(HotelController.class,
				ReservationResource.class);
		List<ReservationResource> resources = assembler.toResources(reservations);
		for(ReservationResource resource : resources ){
			resource.add(assembler.getSelfLink(hotelId, roomId, resource.getReservationId()));
			resource.add(assembler.getReservationsLink(hotelId, roomId));
			resource.add(assembler.getRoomLink(hotelId, roomId));
		}
		return new ResponseEntity<List<ReservationResource>>(resources, HttpStatus.OK);
	}
	
	@RequestMapping("/hotels/{hotel_id}/rooms/{room_id}/reservations/{clientId}")
	public HttpEntity<List<ReservationResource>> showBookedRooms(@PathVariable int hotelId,
			@PathVariable int roomId, @PathVariable int clientId){
		List<Reservation> reservations = reservationDAO.readClientReservations(clientId);
		ReservationAssembler assembler = new ReservationAssembler(HotelController.class,
				ReservationResource.class);
		List<ReservationResource> resources = assembler.toResources(reservations);
		for(ReservationResource resource : resources ){
			resource.add(assembler.getSelfLink(hotelId, roomId, resource.getReservationId()));
			resource.add(assembler.getReservationsLink(hotelId, roomId));
			resource.add(assembler.getRoomLink(hotelId, roomId));
		}
		return new ResponseEntity<List<ReservationResource>>(resources, HttpStatus.OK);
	}
	
	@RequestMapping("/hotels/{hotel_id}/rooms/{room_id}/reservations/{reservation_id}")
	public HttpEntity<ReservationResource> showReservation(@PathVariable int hotelId,
			@PathVariable int roomId, @PathVariable int reservationId){
		Reservation reservation = reservationDAO.readReservation(reservationId);
		ReservationAssembler assembler = new ReservationAssembler(HotelController.class,
				ReservationResource.class);
		ReservationResource resource = assembler.toResource(reservation);
		resource.add(assembler.getSelfLink(hotelId, roomId, reservationId));
		return new ResponseEntity<ReservationResource>(resource, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/hotels/{hotel_id}/rooms/{room_id}/reservations/", 
			method = RequestMethod.POST)
	public HttpEntity<Void> bookRoom(@PathVariable int hotelId,
			@PathVariable int roomId, @RequestBody Reservation reservation){
		int reservationId = reservationDAO.createReservation(reservation);
		final HttpHeaders headers = new HttpHeaders();
		ReservationAssembler assembler = new ReservationAssembler(HotelController.class,
				ReservationResource.class);
	    final Link link = assembler.getSelfLink(hotelId, roomId, reservationId);
	    headers.add("location", link.getHref());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/hotels/{hotel_id}/rooms/{room_id}/reservations/{reservation_id}", 
			method = RequestMethod.DELETE)
	public HttpEntity<Void> cancelReservation(@PathVariable int hotelId,
			@PathVariable int roomId, @PathVariable int reservationId){
		reservationDAO.deleteReservation(reservationId);
		return ResponseEntity.noContent().build();
	}
	
	@RequestMapping(value = "/clients", method = RequestMethod.POST)
	public HttpEntity<Void> registrate(@RequestBody Client client){
		int clientId = clientDAO.createClient(client);
		final HttpHeaders headers = new HttpHeaders();
		ClientAssembler assembler = new ClientAssembler(HotelController.class,
				ClientResource.class);
	    final Link link = assembler.getSelfLink(clientId);
	    headers.add("location", link.getHref());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	
	@RequestMapping("/clients/{client_id}")
	public HttpEntity<ClientResource> showClient(@PathVariable int id){
		Client client = clientDAO.readClient(id);
		ClientAssembler assembler = new ClientAssembler(HotelController.class,
				ClientResource.class);
		ClientResource resource = assembler.toResource(client);
		return new ResponseEntity<ClientResource>(resource, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/clients/{client_id}", method = RequestMethod.PUT)
	public HttpEntity<ClientResource> updateClient(@PathVariable int id, 
			@RequestParam(value = "name", required = false) String name, 
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "age", required = false, defaultValue="0") int age,
			@RequestParam(value = "gender", required = false) String gender,
			@RequestParam(value = "mail", required = false) String mail){
		
		Client client = clientDAO.readClient(id);
		if(name!=null){
			client.setName(name);
		}
		if(surname!=null){
			client.setSurname(surname);
		}
		if(age!=0){
			client.setAge(age);
		}
		if(gender!=null){
			client.setGender(gender);
		}
		if(mail!=null){
			client.setMail(mail);
		}
		
		clientDAO.updateClient(client);
		ClientAssembler assembler = new ClientAssembler(HotelController.class,
				ClientResource.class);
		ClientResource resource = assembler.toResource(client);
		return new ResponseEntity<ClientResource>(resource, HttpStatus.OK);
	}
	
}
