package by.shpakovsky.hotelsservice.main;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;

import by.shpakovsky.hotelsservice.dao.*;
import by.shpakovsky.hotelsservice.dao.impl.*;

@Configuration
public class BeanConfig {

	public BeanConfig() {
		
	}
	
	@Bean
	public HotelDAO hotelDAO(){
		return new HotelDAOimpl();
	}
	
	@Bean
	public RoomDAO roomDAO(){
		return new RoomDAOimpl();
	}
	
	@Bean
	public ReservationDAO reservationDAO(){
		return new ReservationDAOimpl();
	}
	
	@Bean
	public ClientDAO clientDAO(){
		return new ClientDAOimpl();
	}
	
	@Bean
	public Cluster cluster() {
		return Cluster.builder().addContactPoint("127.0.0.1").build();
	}
	
	@Bean
	public Session session() {
		return cluster().connect("bogdan");
	}

}
