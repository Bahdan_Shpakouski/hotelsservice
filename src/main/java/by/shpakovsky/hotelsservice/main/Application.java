package by.shpakovsky.hotelsservice.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import by.shpakovsky.hotelsservice.controller.HelloController;
import by.shpakovsky.hotelsservice.controller.HotelController;

@SpringBootApplication(scanBasePackageClasses={HelloController.class, 
		HotelController.class, BeanConfig.class})
public class Application {

	public Application() {
		
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
