������ �� ��� ������� � ������� ������ HATEOAS JSON.
��� �������� ����� �������������� ������ ������������ � ����������.
���� ������ ����� ������� � ����� �������� � ���� type.
���� ������, ������� ����� ��������������: 
200 - Ok, 201 - Created, 404 � Not found, 400 � Bad Request.
--------------------------------------------------------------
1. ���� - ������������ �������.
2. ������ - Rest, ������ ��� ��� ������� ��� ��� ����� ����� ����������� ��� ������ ����
3. �������: hotel, room, reservation, client

1) hotel
   {
      type: hotel,
      hotel_id: int, 
      hotel_name : string, 
      city: string, 
      address: string, 
      stars: int,
      rooms: Array[room]
      links: {
      	root:  {method: GET, URL: /hotels},
        self: {method: GET, URL: /hotels/hotel_id},
        rooms: {method: GET, URL: /hotels/hotel_id/rooms},
        registrate: {method: POST, URL: /clients}
      }
   }

2) room
   {  
      type: room
      room_id: int, 
      hotel_id: int, 
      room_number: int,
      room_type: string, 
      cost: double,
      reservations: Array[reservation].
      links: {
        root: {method: GET, URL: /hotels},
        hotel: {method: GET, URL: /hotels/hotel_id},
      	parent: {method: GET, URL: /hotels/hotel_id/rooms},
        self: {method: GET, URL: /hotels/hotel_id/rooms/room_id},
        reservations: {method: GET, URL: /hotels/hotel_id/rooms/room_id/reservations},
        book_room: {method: POST, URL: hotels/hotel_id/rooms/room_id/reservations}
      }
   }

3) reservation
   {
      type: reservation
      reservation_id: int,
      room_id: int,
      client_id: int,  
      arrival_date: date, 
      number_of_days: int
      links: {
      	root: {method: GET, URL: /hotels},
        room: {method: GET, URL: /hotels/hotel_id/rooms/room_id},
      	parent: {method: GET, URL: hotels/hotel_id/rooms/room_id/reservations},
        self: {method: GET, URL: /hotels/hotel_id/rooms/room_id/reservations/reservation_id},
        client: {method: GET, URL: /clients/client_id},
	cancel: {method: DELETE, URL: /hotels/hotel_id/rooms/room_id/reservations/reservation_id}
      }
   }

4) client
   {
     type: client
     client_id: int, 
     client_name: string, 
     client_surname: string, 
     client_age: int, 
     client_mail_address: string, 
     client_gender: string 
     links: {
      	hotels: {method: GET, URL: /hotels},
        self: {method: GET, URL:/clients/client_id},
        update: {method: PUT, URL:/clients/client_id},
        show_reservations: {method: GET, URL:/hotels/hotel_id/rooms/room_id/reservations?client_id=client_id}
      }
   }


4. ��������: 
   Show hotels (method - Get, url - /hotels)
   Show hotel (method - Get, url - /hotels/hotel_id)
   Show hotel rooms (method - Get, url - /hotels/hotel_id/rooms)
   Show room (method - Get, url - /hotels/hotel_id/rooms/room_id)
   Show room reservations (method - Get, url - /hotels/hotel_id/rooms/room_id/reservations)
   Show booked room (method - Get, url - /hotels/hotel_id/rooms/room_id/reservations?client_id=client_id)
   Book room (method - Post, url - /hotels/hotel_id/rooms/room_id/reservations/reservation_id)
   Cancel reservation (method - Delete, url - /hotels/hotel_id/rooms/room_id/reservations/reservation_id)
   Registrate (method - Post, url - /clients)
   Show client information (method - Get, url - /clients/client_id)
   Update client information (method - Put, url - /clients/client_id)
5. ����� ��������:
   ������� �������������� �� �����, ������������� ����� � ������, � ����������� ���� ��������� ������(�� ��������������� �����) �� �����-�� ����.
   ����� ����������� ������ 1 ����� �� ���, ����� �������� �����.